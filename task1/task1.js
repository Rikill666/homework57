const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'}
];

console.log("Total amount of time spent working on tasks from the 'Frontend' category: ");
console.log(tasks.reduce((acc, task) => {if(task.category === 'Frontend') acc += task.timeSpent; return acc}, 0));

console.log("The total amount of time spent working on tasks such as 'bug': ");
console.log(tasks.reduce((acc, task) => {if(task.type === 'bug') acc += task.timeSpent; return acc}, 0));

console.log("The number of tasks that have the word 'UI' in the title: ");
console.log(tasks.filter(t => t.title.includes('UI')).length);

console.log("Number of tasks in each category: ");
console.log(tasks.reduce((acc, task) => {acc[task.category]++; return acc}, {'Frontend': 0, 'Backend': 0}));

console.log("Array of tasks with time spent over 4 hours: ");
console.log(tasks.reduce((acc, task) => {task.timeSpent > 4 && acc.push({title: task.title, category: task.category}); return acc}, []));




