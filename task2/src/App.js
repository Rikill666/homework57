import React from 'react';
import './App.css';

import ListBuilder from "./Containers/ListBuilder/ListBuilder";

function App() {
  return (
    <div className="App">
        <ListBuilder/>
    </div>
  );
}

export default App;
