import React from 'react';
import './EnterForm.css'
const EnterForm = (props) => {
    return (
        <div className="addBlock">
            <input id="addProductName" placeholder="Enter name" className="inputName inp" type="text" onChange={props.currentProductName}/>
            <input id="addProductPrice" placeholder="Price" className="inputPrice inp" type="number" min="1" onChange={props.currentProductPrice}/>KGS
            <button className="addButton" onClick={props.createProduct}>Add</button>
        </div>
    );
};

export default EnterForm;