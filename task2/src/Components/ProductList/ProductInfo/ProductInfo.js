import React from 'react';
import './ProductInfo.css';

const ProductInfo = (props) => {
    return (
        <li>
            <p>{props.product.name} </p>
            <p className="priceProduct">{props.product.price} KGS <i onClick={props.remove} className="fas fa-times"/></p>
        </li>
    );
};

export default ProductInfo;