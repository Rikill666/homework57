import React from 'react';
import ProductInfo from "../ProductInfo/ProductInfo";

const ProductsBlock = ({products, deleteProduct}) => {
    const ulStyle = {
        border: '1px solid black',
        paddingLeft:'5px',
        width: '50%',
        margin: '10px auto'
    };
    return (
        <ul style={ulStyle}>
            {products.map(product =>
                <ProductInfo
                    key={product.id}
                    remove={() => deleteProduct(product.id)}
                    product={product}
                />
            )}
        </ul>
    );
};

export default ProductsBlock;