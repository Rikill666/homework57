import React from 'react';

const TotalPrice = ({price}) => {
    return (
        <h5>
            Total spent: {price} KGS
        </h5>
    );
};

export default TotalPrice;