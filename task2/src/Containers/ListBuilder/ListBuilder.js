import React, {Component} from 'react';
import nanoid from 'nanoid';
import EnterForm from "../../Components/ProductList/EnterForm/EnterForm";
import ProductsBlock from "../../Components/ProductList/ProductsBlock/ProductsBlock";
import TotalPrice from "../../Components/ProductList/TotalPrice/TotalPrice";

class ListBuilder extends Component {
    state = {
        products:[],
        price: 0,
        newProduct:{name:"", price:""}
    };
    currentProductName = event => {
        let newProduct= {...this.state.newProduct};
        newProduct.name = event.target.value;
        this.setState({newProduct});
    };
    currentProductPrice = event => {
        const newProduct = {...this.state.newProduct};
        newProduct.price = event.target.value;
        if(parseInt(event.target.value) > 0){
            this.setState({newProduct});
        }

    };
    createProduct = () => {
        if (this.state.newProduct.name && this.state.newProduct.price) {
            let products = [...this.state.products];
            const product = {...this.state.newProduct, id: nanoid()};
            products.push(product);
            document.getElementById("addProductName").value = "";
            document.getElementById("addProductPrice").value = "";
            let price = this.state.price;
            price += parseInt(this.state.newProduct.price);
            let newProduct = {...this.state.newProduct};
            newProduct.name="";
            newProduct.price="";
            this.setState({products, newProduct, price});
        }
    };
    deleteProduct = id => {
        const index = this.state.products.findIndex(t => t.id === id);
        const products = [...this.state.products];
        let price = this.state.price;
        price-=products[index].price;
        products.splice(index, 1);
        this.setState({products, price});
    };
    render() {
        return (
            <div>
                <EnterForm createProduct={this.createProduct} currentProductName={this.currentProductName} currentProductPrice={this.currentProductPrice}/>
                <ProductsBlock products={this.state.products} deleteProduct={this.deleteProduct}/>
                <TotalPrice price={this.state.price}/>
            </div>
        );
    }
}

export default ListBuilder;